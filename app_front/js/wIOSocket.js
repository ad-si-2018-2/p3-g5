var iosocket = io.connect();
iosocket.on('connect', function() {
  showMsg('Aguardando inscrição na sala');
  iosocket.on('connected', function(data) {
    showMsg('Conectado');
  });
  iosocket.on('message', function(data) {
    showMsg(fmtMsgRecebida(data.timestamp, data.from, data.msg));
  });

  iosocket.on('disconnect', function() {
    showMsg('Disconnected');
  });
});

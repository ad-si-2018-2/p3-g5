const amqp = require('../middleware/rabbitMQ');

module.exports = {
  send: (irc_client, msg, params) => {
    switch (msg) {
      case 'NICK':
        try {
          amqp.enviarParaFila('commands', { command: 'NICK', params });
          //irc_client.send(msg, params.newNick);
          amqp.receberDaFila('commands', ({command, params}) => {
            irc_client.send(command, params.newNick)
          });
          return true;
        } catch (error) {
          console.log('ERROR: ' + error);
          return false;
        }
        break;
      case 'PART':
        amqp.enviarParaFila('commands', { command: 'PART', params });
        // irc_client.send(msg, params.channel);
        amqp.receberDaFila('commands', ({command, params}) => {
          irc_client.send(command, params.channel)
        });
        break;
      case 'QUIT':
        amqp.enviarParaFila('commands', { command: 'QUIT', params });
        // irc_client.send(msg, params.nick);
        amqp.receberDaFila('commands', ({command, params}) => {
          irc_client.send(command, params.nick)
        });
        break;
      case 'KICK':
        amqp.enviarParaFila('commands', { command: 'KICK', params });
        // irc_client.send(msg, params.canal, params.target);
        amqp.receberDaFila('commands', ({command, params}) => {
          irc_client.send(command, params.canal, params.target)
        });
        break;
      case 'MODE':
        amqp.enviarParaFila('commands', { command: 'MODE', params });
        // irc_client.send(msg, params.target, params.mode, params.by);
        amqp.receberDaFila('commands', ({command, params}) => {
          irc_client.send(command, params.target, params.mode, params.by)
        });
        break;

      case 'MOTD':
        amqp.enviarParaFila('commands', { command: 'MOTD' });
        // irc_client.send(msg);
        amqp.receberDaFila('commands', ({command}) => {
          irc_client.send(command)
        });
        break;

      case 'JOIN':
        amqp.enviarParaFila('commands', {command : 'JOIN', params});

        amqp.receberDaFila('commands', ({command, params}) => {
          if(command == 'JOIN')
            irc_client.join(params.channel);
        });
        break;
      
      case 'PRIVMSG':
        amqp.enviarParaFila('commands', {command : 'PRIVMSG', params});

        amqp.receberDaFila('commands', ({command, params}) => {
          if(command == 'PRIVMSG')
            irc_client.say(params.to, params.msg);
        });
      break;

    }
  },

  //target pode ser #canal ou usuário no caso de privmsg
  say: (irc_client, target, msg) => {
    irc_client.say(target, msg);
  },

  // join: (irc_client, params) => {
  //   amqp.enviarParaFila('commands', {command: 'JOIN'});
  //   irc_client.join(params.channel);
  // },

  emmit: socket => {
    //  amqp.enviarParaFila(canal, data);
    amqp.receberDaFila(amqp.getCanal().canal, data => {
      socket.emit('message', data);
    });
  }
};

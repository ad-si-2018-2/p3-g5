module.exports = [
  '+ - Adiciona modo',
  '- - Remove modo',
  'a - Torna o usuário operador',
  'b - Banir o usuário do canal (Requer privilegio de operador)',
  'i - Canal apenas para convidados',
  'i - Torna o usuário invisível',
  'l - Canal limitado a numero de usuários',
  'm - Canal moderado, apenas operadores de canal podem falar',
  'm - Torna o usuário operador',
  'n - Mensagens externas não são permitidas',
  'o - Torna o usuário operador de canal',
  'p - Canal privado',
  's - Canal secreto',
  's - Usuário recebe notícias do servidor',
  't - Canal limitado a topico - Apenas operador pode alterar',
  'w - Recebe mensagens do operador'
];

//possíveis erros que o servidor pode enviar
module.exports = errCode => {
  switch (errCode) {
    case 400:
      return 'Desconhecido';
      break;
    case 401:
      return 'Nickname inexistente';
      break;
    case 402:
      return 'Servidor inexistente';
      break;
    case 403:
      return 'Canal inexistente';
      break;
    case 404:
      return 'Não foi possível enviar mensagem ao canal';
      break;
    case 405:
      return 'Você Entrou em muitas salas';
      break;
    case 421:
      return 'Comando desconhecido';
      break;
    case 422:
      return ':MOTD arquivo não encontrado';
      break;
    case 432:
      return 'Nickname inválido';
      break;
    case 433:
      return 'Nickname em uso';
      break;
    case 441:
      return 'Usuário não encontrado no canal';
      break;
    case 442:
      return 'Você não está nesse canal';
      break;
    case 443:
      return 'Usuário já está no canal';
      break;
    case 451:
      return 'Você não está registrado';
      break;
    case 461:
      return 'Parametros insuficientes';
      break;
    case 462:
      return 'Você não pode re-registrar';
      break;
    case 464:
      return 'Senha incorreta';
      break;
    case 464:
      return 'Você está banido desse servidor';
      break;
    case 471:
      return 'Não foi possível entrar no canal';
      break;
    case 472:
      return 'Modo inválido';
      break;
    case 473:
      return 'Só é possível entrar no canal CONVIDADOS';
      break;
    case 474:
      return 'Você foi banido desse canal';
      break;
    case 475:
      return 'Chave para entrar no canal inválida ou desconhecida';
      break;
    case 481:
      return 'Você não é um irc operador';
      break;
    case 482:
      return 'Você não é um operador do canal';
      break;
    case 483:
      return 'Você não pode (kill) encerrar o servidor';
      break;
    case 491:
      return 'Não há conexões com o servidor';
      break;
    case 501:
      return 'Marcador mode inválido';
      break;
    case 502:
      return 'Não é possível alterar o modo de outro usuário';
      break;
    case 691:
      return 'TLS handshake falhou';
      break;
    case 723:
      return 'Privilégios de operador insuficientes';
      break;
    case 902:
      return 'Você precisa ter um Nickname associado';
      break;
    case 904:
      return 'SASL falha na autenticação';
      break;
    case 905:
      return 'SASL mensagem muito grande';
      break;
    case 906:
      return 'SASL autenticação encerrada';
      break;
    case 907:
      return 'SASL Você já está autenticado';
      break;
    default:
      return 'Não foi possível processar o comando';
  }
};

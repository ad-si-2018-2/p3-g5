//lista de comandos suportados
let commands = [
  '/join < #canal > - Entrar em um canal',
  '/leave < #canal > - Sair de um canal',
  '/kick  < nickname > < #canal > - Expulsar usuário do canal (Requer privilegio de operador)',
  '/motd - Exibe mensagem do dia do servidor',
  '/quit - Desconecta do servidor',
  '/help - Exibe todos os comandos suportados',
  '/help < /comando > - Exibe informações do comando ',
  '/nick < nickname > - Trocar de nickname',
  '/privmsg < usuario > < mesagem > - Enviar mensagem privado ao usuário',
  'mode < modo > < #canal > - Alterar modo do canal',
  '/mode < modo > < nickname > - Alterar modo de um usuário (Requer privilegio de operador)',
  '/mode < modo > - Alterar modo do usuário',
  '/modes - lista os modos disponíveis'
];

module.exports = param => {
  if (param) {
    switch (param) {
      case 'join':
        return commands[0];
        break;
      case 'leave':
        return commands[1];
        break;
      case 'kick':
        return commands[2];
        break;
      case 'motd':
        return commands[3];
        break;
      case 'quit':
        return commands[4];
        break;
      case 'help':
        return [commands[5], commands[6]];
        break;
      case 'nick':
        return commands[7];
        break;
      case 'privmsg':
        return commands[8];
        break;
      case 'mode':
        return [commands[9], commands[10], commands[11], commands[12]];
        break;
      default:
        return 'Comando inválido';
    }
  } else {
    return commands;
  }
};

const amqp = require('amqplib/callback_api');
const { rabbitServer } = require('../config/config');
let id_canal = {};
let amqp_conn = '';
let amqp_ch = '';

module.exports = {
  setCanal: id => {
    id_canal['canal'] = id;
    console.log(id);
  },
  getCanal: () => {
    return id_canal;
  },
  connect: () => {
    amqp.connect(
      rabbitServer,
      (err, conn) => {
        console.log('Conectou AMQP Server');
        conn.createChannel((err, ch) => {
          console.log('Canal criado');
          amqp_conn = conn;
          amqp_ch = ch;
        });
      }
    );
  },
  receberDaFila: (fila, callback) => {
    amqp_ch.consume(
      fila,
      function(msg) {
        console.log('Received %s', msg.content.toString());
        callback(JSON.parse(msg.content.toString()));
      },
      { noAck: true }
    );
  },

  enviarParaFila: (fila, msg) => {
    msg = new Buffer(JSON.stringify(msg));
    amqp_ch.assertQueue(fila, { durable: false });
    amqp_ch.sendToQueue(fila, msg);
    console.log('Sent to ' + fila + ' : ' + msg);
  }
};
